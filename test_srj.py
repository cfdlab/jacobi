"""Test script for the Cython version of the SRJ scheme"""

import sys

# Dynamically import Cython code
import pyximport
pyximport.install()

import jacobi_cython as jc

# initial solution matrix
import matrix_solve as ms

# Python Option parser
from optparse import OptionParser, OptionGroup, Option

# SRJ scheduler
from srj_scheduler import SRJScheduler

# Python timer
from time import time

if __name__ == '__main__':
    usage = """
        python %prog [options] """
    parser = OptionParser(usage)

    # -s, --size
    parser.add_option(
        "-s", "--size", dest="size", type="int", default=32,
        help="""Problem size (nx X ny) nodes/cells. Default 64""")
    
    # -p, --levels
    parser.add_option(
        "-p", "--levels", dest="levels", type="int", default=3,
        help="""Number of levels in the SRJ scheme """
        )

    # --tol
    parser.add_option(
        "--tol", dest="tolerance", type="float", default=1e-8,
        help="""Iterative solution tolerance. Default (1e-6)""")

    # get the options from the command line
    (options, args) = parser.parse_args(sys.argv)
    size = options.size
    p = options.levels 
    tol = options.tolerance   

    # run the GS iterations for reference
    t1 = time()
    T = ms.get_solution_matrix(size+2, size+2); Tnew = T.copy()
    gs_niter = jc.gauss_seidel( T, Tnew, size, tol )
    gs_time = time() - t1

    # run the SRJ iterations for comparison
    t1 = time()
    T = ms.get_solution_matrix(size+2, size+2); Tnew = T.copy()

    srj = SRJScheduler(size=size)
    w, q = srj.get_relaxation_vectors(p)

    srj_niter = jc.scheduled_relaxation_jacobi(T, Tnew, w, q, size, tol)
    srj_time = time() - t1

    print gs_time/srj_time, gs_niter/float(srj_niter)

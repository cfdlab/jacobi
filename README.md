Welcome to Jacobi!
-------------------

A suite of Python programs for `fast` solution methods for the
Poisson/Laplace equation.

Installation
=============

You don't have to install this package. You just need a working Python
environment, preferably [Enthought
Canopy](https://www.enthought.com/products/canopy/), which will give
you SciPy and the associated sparse linear solvers.

Usage
======

You can compare the results for the SRJ iterations over a regular Gauss-Seidel scheme using `python test_srj.py --levels=5 --size=256`. Or, you can try the IPython [notebook](http://nbviewer.ipython.org/urls/bitbucket.org/cfdlab/jacobi/raw/master/srj_example.ipynb)
"""Utility to get the matrix representation of the Poisson system"""

# timer
from time import time

# NumPy
import numpy

# Sparse matrices and solvers using SciPy
HAVE_SCIPY=True
try:
    import scipy
except ImportError:
    HAVE_SCIPY=False
    
if HAVE_SCIPY:
    from scipy import linalg
    from scipy import sparse
    from scipy.sparse.linalg import dsolve,\
        bicg, gmres, cg, minres
else:
    from numpy import linalg

# available sparse solvers from SciPy
if HAVE_SCIPY:
    sparse_solvers = {
        'DIRECT': dsolve.spsolve,
        'BICG': bicg,
        'GMRES': gmres,
        'CG': cg,
        'MINRES': minres
        }

def flatten(i, j, nx, ny):
    """Flatten a 2D matrix index (ij) using row-ordering"""
    return int( i*nx + j )

def get_sparse_matrix(ncells):
    """Get the coefficient matrix in sparse form.

    We use the linked-list matrix to easily populate the sparse
    matrix. This is later converted to a CSR matrix for solution.
    
    """
    # coefficient matrix and rhs vector
    A = sparse.lil_matrix( (ncells, ncells), dtype=numpy.float64 )
    b = numpy.zeros( shape=(ncells,) )

    return A, b

def get_dense_matrix(ncells):
    """Get the coefficient matrix in dense form"""
    # coefficient matrix and rhs vector
    A = numpy.zeros( shape=(ncells, ncells) )
    b = numpy.zeros( shape=(ncells,) )

    return A, b

def get_solution_matrix(imax, jmax):
    "Return the solution matrix with a default boundary condition"
    T = numpy.zeros( shape=(imax, jmax) )

    # boundary conditions. T = 1 along y = 0 (bottom)
    T[0][:] = 1.0

    return T

def assemble(T, A, b):
    """Assemble the coefficient matrix and the solution vector.

    The solution is required for the "interior" domain of T. Each
    cell/node corresponds to a row in the coefficient matrix. The
    column for the entry is determined by the flattened index.

    """

    imax, jmax = T.shape
    nx = jmax-2; ny = imax-2

    row = -1
    for i in range(1, imax-1):
        for j in range(1, jmax-1):
            row += 1

            # the diagonal values are -4
            A[row, row] = -4.0
            
            # stencil points
            im = i-1; ip = i + 1
            jm = j-1; jp = j + 1

            # boundary condition for the bottom face
            if im > 0:
                A[row, flatten(im-1, j-1, nx, ny)] = 1
            else:
                b[row] += -T[im, j]

            # boundary condition for the top face
            if ip < imax-1:
                A[row, flatten(ip-1, j-1, nx, ny)] = 1
            else:
                b[row] += -T[ip, j]

            # boundary condition for the left face
            if jm > 0:
                A[row, flatten(i-1, jm-1, nx, ny)] = 1
            else:
                b[row] += -T[i, jm]
                
            # boundary condition for the right face
            if jp < jmax-1:
                A[row, flatten(i-1, jp-1, nx, ny)] = 1
            else:
                b[row] += -T[i, jp]

def solve_system(nx, ny,
    sparse=False, solver='DIRECT', tol=1e-6):
    
    # get the total number of solution points and the range of indices
    ncells = nx*ny
    imax = ny+2; jmax = nx+2

    # get the initial solution matrix
    T = get_solution_matrix(imax, jmax)

    # get the coefficient matrix in either dense or sparse form
    if sparse:
        A, b = get_sparse_matrix(ncells)
    else:
        A, b = get_dense_matrix(ncells)

    # assemble the coefficient matrix
    assemble(T, A, b)
    
    # solve
    if sparse and HAVE_SCIPY: 
        A = A.tocsr()
        _solver = sparse_solvers[ solver ]

        # solve the sparse system
        t1 = time()

        if solver != 'DIRECT':
            x, info = _solver(A, b, tol=tol)
        else:
            x = _solver(A, b)
        
        time_matrix_solution = time()-t1

    else:
        t1 = time()
        x = linalg.solve(A, b)
        
        time_matrix_solution = time()-t1

    # put the solution back into T
    x.shape = ny, nx
    T[1:-1, 1:-1][:] = x
    
    return T, time_matrix_solution

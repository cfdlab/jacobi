"""Cython implementation of the Jacobi schemes"""

# NumPy
import numpy as np
cimport numpy as np

# Cython
cimport cython

# math.h functions
from libc.math cimport sqrt, fabs

# SRJ scheduler
from srj_scheduler import SRJScheduler

@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline double norm1(
    double[:, :] arr1, double[:, :] arr2, int imax, int jmax):

    cdef int i, j
    cdef double norm = 0.0

    for i in range(imax):
        for j in range(jmax):
            norm = norm + fabs( arr1[i, j] - arr2[i, j] )

    return norm

@cython.boundscheck(False)
@cython.wraparound(False)
cpdef int gauss_seidel(
    np.ndarray[ndim=2, dtype=np.float64_t] T,
    np.ndarray[ndim=2, dtype=np.float64_t] Tnew,
    int size, double tol):
    r"""Gauss Seidel Iterations in Python
    
    Parameters:
    
    T, Tnew : np.ndarray
        Solution buffers

    size : int
        Size of the square grid. Only physical nodes are counted.

    tol : double
        Error tolerance

    """
    # locals
    cdef int ncells = size*size
    cdef int imax = size + 2
    cdef int jmax = size + 2

    cdef int iteration = 0
    cdef double error = 1.0

    cdef int i, j

    while( error > tol ):
        for i in range(1, imax-1):
            for j in range(1, jmax-1):

                Tnew[i, j] = 0.25 * \
                    ( Tnew[i+1,j] + Tnew[i-1,j] + Tnew[i,j+1] + Tnew[i,j-1] )

        # check for convergence in the 1 Norm
        error = norm1( T, Tnew, imax, jmax )
        if error < tol:
            break

        # swap buffers
        T[:] = Tnew[:]

        # increment iteration count
        iteration += 1

    # return the number of iterations
    return iteration

@cython.boundscheck(False)
@cython.wraparound(False)
cpdef scheduled_relaxation_jacobi(
    np.ndarray[ndim=2, dtype=np.float64_t] T,
    np.ndarray[ndim=2, dtype=np.float64_t] Tnew,
    np.ndarray[ndim=1, dtype=np.float64_t] w,
    np.ndarray[ndim=1, dtype=np.int64_t] q,
    int size, double tol):
    """Solution using the SRJ algorithm"""

    # get the number of cells and index range
    cdef int ncells = size*size
    cdef int imax = size+2
    cdef int jmax = size+2

    # get the relaxation schedule
    cdef object srj = SRJScheduler(size=size)
    cdef np.ndarray[ndim=1, dtype=np.float64_t] wt = srj.get_relaxation_schedule(w, q)

    # number of 'M' or inner iterations
    cdef int m = wt.size

    # locals
    cdef int i, j, niter_inner, niter_total = 0
    cdef bint break_outer = False
    cdef double error = 1.0
    cdef double omega, omega4

    while( error > tol ):
        # 'M' iterations with prescribed omega
        for niter_inner in range(m):
            niter_total += 1

            omega = wt[ niter_inner ]
            omega4 = 0.25*omega
        
            # update the nodes
            for i in range(1, imax-1):
                for j in range(1, jmax-1):
                    Tnew[i, j] = (1-omega)*T[i, j] + omega4 * \
                        ( T[i-1, j] + T[i+1, j] + T[i, j-1] + T[i, j+1] )
                    
            # check for convergence in the 1 Norm and indicate that
            # the inner loop has converged and we want to break
            error = norm1( T, Tnew, imax, jmax )
            if error < tol:
                break_outer=True
                break

            # swap buffers for next iteration
            T[:] = Tnew[:]

        # break out of the while loop
        if break_outer: break

    # return the total number of iterations
    return niter_total

"""Solution of the Poisson/Laplace equation del^2 T = b on the unit
square [0, 1] X [0, 1] with Dirichlet boundary conditions.

Example Usage:

  $ python poisson.py --size=64 --solution-method=JACOBI --tol=1e-8 -g

where, -g is used to pop up a Matplotlib window for the solution.

The problem is discretized using (size X size) cells, with nx cells
along the 'y' coordinate direction and ny cells along the 'x'
coordinate direction. A standard five point stencil is used for the
discretization which necessitates one ghost cell at either end of the
domain.

The solution T is a 2D-array with shape (0:ny+2, 0:nx+2). Boundary
values are specified along:

T[0, :]     : bottom  (y=0)
T[-1,:]     : top     (y=1)
T[:, 0]     : left    (x=0)
T[:,-1]     : right   (x=1)

For a solution of the resulting system Ax = b, the coefficient matrix
A is of shape (ncells, ncells) where ncells = nx * ny

The RHS vector b is of shape (ncells, 1)

"""
# sys
import sys

# timer
from time import time

# NumPy for arrays
import numpy
from numpy import linalg

# Module to include the Matrix solution methods
from matrix_solve import HAVE_SCIPY, sparse_solvers
import matrix_solve

# Python Option parser
from optparse import OptionParser, OptionGroup, Option

# Matplotlib for plotting
HAVE_PYLAB=True
try:
    from matplotlib import pylab
    import matplotlib.pyplot as plt
except ImportError:
    HAVE_PYLAB=False

def numpy_jacobi(nx, ny, tol=1e-12):
    """Solution using Jacobi iterations"""

    # get the number of cells and index range
    ncells = nx*ny
    imax = ny+2; jmax = nx+2

    T = matrix_solve.get_solution_matrix(imax, jmax)
    _T = T.copy()

    t1 = time()
    niter = 1000000
    iteration = 0
    while( iteration < niter ):
        _T[1:-1, 1:-1] = 0.25 * ( _T[0:-2, 1:-1] + \
                                      _T[2:, 1:-1] + \
                                      _T[1:-1, 0:-2] + \
                                      _T[1:-1, 2:] )
        
        # check for convergence in the 1 Norm
        norm = linalg.norm( T - _T, 1 )
        if norm < tol:
            break

        # swap buffers
        T[:] = _T[:]

        iteration += 1

    t2 = time() - t1
            
    return T, t2, iteration

def _print(solver, time, norm=0.0, num_iterations=None):
    print 'Solver :: %s, Time :: %g, Error :: %g, Iterations :: %s'%(
        solver, time, norm, num_iterations)

if __name__ == '__main__':
    # Add some options to a command line parser
    usage = """
        python %prog [options] """
    parser = OptionParser(usage)

    # -s, --size
    parser.add_option(
        "-s", "--size", dest="size", type="int", default=32,
        help="""Problem size (nx X ny) nodes/cells. Default 64""")

    # --solution-method
    parser.add_option(
        "--solution-method", dest="solution_method", type="str",
        default="CG", 
        help="""Solution method. Available solvers = %s"""%sparse_solvers.keys()
        )

    # --tol
    parser.add_option(
        "--tol", dest="tolerance", type="float", default=1e-8,
        help="""Iterative solution tolerance. Default (1e-6)""")

    # -g
    parser.add_option(
        "-g", dest="graphics", action="store_true", default=False,
        help="""Pop up matplotlib graphics. Defalut False""")

    # get the options from the command line
    (options, args) = parser.parse_args(sys.argv)

    # Problem size and number of cells
    size = options.size
    nx = ny = size
    
    # Get the reference solution using Jacobi iterations
    Tj, time_jacobi, num_jacobi_iterations = numpy_jacobi(
        nx, ny, options.tolerance)
    
    _print( 'NumPy Jacobi', time_jacobi, 0.0, num_jacobi_iterations )

    # Solve using the requested method
    solution_method = options.solution_method

    if HAVE_SCIPY:
        if solution_method not in sparse_solvers:
            available_solvers = sparse_solvers.keys()
            raise RuntimeError('Sparse solver %s not available. Avaliable solvers: %s'%(solution_method, available_solvers))
        
        Ts, time_matrix_solution = matrix_solve.solve_system(
            nx, ny, sparse=True, solver=solution_method, tol=options.tolerance)
        
        inf_norm = linalg.norm( Ts-Tj, numpy.inf )
        _print( 'Sparse %s'%solution_method, time_matrix_solution, inf_norm )
            
    else:
        raise RuntimeError("Package SciPy not available. Cannot use sparse solver")        

    # plot the solution
    if ( HAVE_PYLAB and options.graphics ):
        fig, axes = plt.subplots(nrows=1, ncols=2)

        ax1, ax2 = axes.flat
        
        ax1 = pylab.subplot(121)
        im1 = ax1.imshow(Tj, origin='lower left')
        ax1.set_title('JACOBI (%d, %gs) '%(num_jacobi_iterations, time_jacobi))
        
        ax2 = pylab.subplot(122)
        im2 = ax2.imshow(Ts, origin='lower left')
        ax2.set_title("%s (%gs)"%(options.solution_method, time_matrix_solution))

        fig.subplots_adjust(right=0.8)
        cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
        fig.colorbar(im1, cax=cbar_ax)

        # pop-up
        pylab.show()

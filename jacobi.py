"""Functions for different kinds of Jacobi iterations. 

These are written in pure Python and therefore will be considerably
slower than the NumPy version (see `poisson.py`) or optimized
Cython/f2py versions including matrix solution methods using SciPy's
sparse linear algebra routines.

Usage: $ python jacobi.py

This runs a simple problem on a 32 X 32 grid and prints out difference
in solution and number of iterations used.

"""
# NumPy
from numpy import linalg
import numpy

# Python timer
from time import time

# Local imports
from matrix_solve import get_solution_matrix
from srj_scheduler import SRJScheduler

# Pandas for pretty printing
HAVE_PANDAS=True
try:
    import pandas as pd
except ImportError:
    HAVE_PANDAS=False

def naive_jacobi(nx, ny, tol=1e-12):
    """Naive Jacobi iterations in Python"""
    # get the number of cells and index range
    size = nx
    ncells = nx*ny
    imax = ny+2; jmax = nx+2

    T = get_solution_matrix(imax, jmax)
    _T = T.copy()
    
    # measure time from here
    t1 = time()

    # Begin the iterations
    niter = 100000
    iteration = 0
    while( iteration < niter ):

        # update the red-nodes
        for i in range(1, imax-1):
            for j in range(1, jmax-1):
                ip1 = i+1; im1 = i-1
                jp1 = j+1; jm1 = j-1
                
                _T[i, j] = 0.25 * (T[im1, j] + \
                                       T[ip1, j] + \
                                       T[i, jm1] + \
                                       T[i, jp1] )

        # check for convergence in the 1 Norm
        norm = linalg.norm( T - _T, 1 )
        if norm < tol:
            break

        # swap buffers
        T[:] = _T[:]

        iteration += 1

    t2 = time() - t1
            
    return T, t2, iteration

def jacobi_red_black_sor(nx, ny, tol=1e-12, omega=1.5):
    """Solution using the Red-black SOR algorithm"""

    # get the number of cells and index range
    size = nx
    ncells = nx*ny
    imax = ny+2; jmax = nx+2

    T = get_solution_matrix(imax, jmax)
    _T = T.copy()
    
    color = numpy.zeros_like( T, dtype=numpy.int32 )
    jrange1 = range(1, jmax-2, 2)
    jrange2 = range(2, jmax-2, 2)

    # measure time from here
    t1 = time()

    # color the nodes
    for i in range(1, imax-1):
        if ( (i % 2) == 1 ): 
            jrange = jrange1
        else:
            jrange = jrange2

        for j in jrange:
            color[i, j] = 1

    # get the boolean arrays for the two nodes
    red = color==1
    blk = color==0

    # Begin the iterations
    niter = 100000
    iteration = 0
    while( iteration < niter ):

        # update the red-nodes
        for i in range(1, imax-1):
            for j in range(1, jmax-1):
                if red[i, j]:
                    ip1 = i+1; im1 = i-1
                    jp1 = j+1; jm1 = j-1

                    _T[i, j] = _T[i, j] + omega * 0.25 * ( _T[im1, j] + \
                                                               _T[ip1, j] + \
                                                               _T[i, jm1] + \
                                                               _T[i, jp1] - \
                                                               4*_T[i, j] )

        # update the black nodes
        for i in range(1, imax-1):
            for j in range(1, jmax-1):
                if blk[i, j]:
                    ip1 = i+1; im1 = i-1
                    jp1 = j+1; jm1 = j-1

                    _T[i, j] = _T[i, j] + omega * 0.25 * ( _T[im1, j] + \
                                                               _T[ip1, j] + \
                                                               _T[i, jm1] + \
                                                               _T[i, jp1] - \
                                                               4*_T[i, j] )

        # check for convergence in the 1 Norm
        norm = linalg.norm( T - _T, 1 )
        if norm < tol:
            break

        # swap buffers
        T[:] = _T[:]

        iteration += 1

    t2 = time() - t1
            
    return T, t2, iteration

def scheduled_relaxation_jacobi(
    nx, ny, w, q, tol=1e-12):
    """Solution using the SRJ algorithm"""

    # get the number of cells and index range
    size = nx
    ncells = nx*ny
    imax = ny+2; jmax = nx+2

    T = get_solution_matrix(imax, jmax)
    _T = T.copy()
    
    # measure time from here
    t1 = time()

    # get the relaxation schedule
    srj = SRJScheduler(size=size)
    wt = srj.get_relaxation_schedule(w, q)

    # number of 'M' or inner iterations
    m = wt.size

    # Begin the outer iterations
    niter = 100000
    iteration = 0
    num_iter_total = 0
    break_iteration = False
    while( iteration < niter ):
        
        # 'M' iterations with prescribed omega
        for inner_iteration in range(m):
            omega = wt[inner_iteration]
            omega4 = 0.25*omega

            # update the nodes
            for i in range(1, imax-1):
                for j in range(1, jmax-1):
                    ip1 = i+1; im1 = i-1
                    jp1 = j+1; jm1 = j-1
                    
                    _T[i, j] = (1-omega)*T[i, j] + omega4 * ( T[im1, j] + \
                                                                  T[ip1, j] + \
                                                                  T[i, jm1] + \
                                                                  T[i, jp1] )
                    
            # check for convergence in the 1 Norm and indicate that
            # the inner loop has converged and we want to break
            error = linalg.norm( T - _T, 1 )
            if error < tol:
                break_iteration=True

            # total number of iterations
            num_iter_total += 1

            # swap buffers
            T[:] = _T[:]

        # break out of the while loop
        if break_iteration: break

        iteration += 1

    t2 = time() - t1
            
    return T, t2, num_iter_total

# simple test for a small problem (size=32)
if __name__ == '__main__':
    nx = ny = 32
    tol = 1e-8
    
    # Standard Jacobi
    sol1, t1, num_iterations1 = naive_jacobi(nx, ny, tol)
    
    # Jacobi with Red-black ordering
    sol2, t2, num_iterations2 = jacobi_red_black_sor(nx, ny, tol, omega=1.0)
    diff_jrb = linalg.norm( sol1-sol2, 1 )

    # Jacobi with SOR
    sol3, t3, num_iterations3 = jacobi_red_black_sor(nx, ny, tol, omega=1.5)
    diff_jrb_sor = linalg.norm( sol1-sol3, 1 )

    # Scheduled relaxation jacobi P5 scheme
    w = [330.57,82.172,13.441,2.2402,0.60810]
    q = [1, 2, 7, 20, 46]

    sol4, t4, num_iterations4 = scheduled_relaxation_jacobi(
        nx, ny, w, q, tol)
    diff_srj = linalg.norm( sol4-sol1, 1)
    
    schemes = ['Jacobi', 'Gauss Seidel', 'Jacobi SOR', 'SRJ']
    times = [t1, t2, t3, t4]
    errors = ['--', diff_jrb, diff_jrb_sor, diff_srj]
    iterations = [num_iterations1, num_iterations2, num_iterations3, num_iterations4]
    
    if HAVE_PANDAS:
        data = dict(time=times, error=errors, num_iteration=iterations)
        data_frame = pd.DataFrame(data=data, index=schemes)
        print data_frame

    else:
        print 'Scheme \t\t Num Iterations \t Time (s)'
        print '%s \t\t %d \t\t\t %g'%('Jacobi', num_iterations1, t1)
        print '%s \t\t %d \t\t\t %g'%('GS', num_iterations2, t2)
        print '%s \t\t %d \t\t\t %g'%('SOR', num_iterations3, t3)
        print '%s \t\t %d \t\t\t %g '%('SRJ', num_iterations4, t4)

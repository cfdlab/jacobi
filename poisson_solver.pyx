"""Cython implementation of the Poisson solver on a 2D Cartesian grid
using the GSSOR and SRJ schemes

Example usage (Cython is needed) for the case of a square grid for the
solution of del^2 phi = -1

>>> import pyximport; pyximport.install()
>>> import poisson_solver as ps
>>> nrows = ncols = 256
>>> dx = dy = 1./nrows
>>> phi = numpy.zeros( (nrows, ncols) )
>>> rhs = -1*numpy.ones_like(phi)
>>> w, q = ps.SRJScheduler.get_relaxation_vectors(size=max(nrows, ncols))
>>> ps.srj(phi, w, q, rhs, dx, dy, nrows, ncols, tol=1e-8, max_iter=100000)

"""

# NumPy
import numpy as np
cimport numpy as np

# Cython
cimport cython

# math.h functions
from libc.math cimport sqrt, fabs

# SRJ scheduler
from srj_scheduler import SRJScheduler

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
cdef inline double norm1(
    double[:, :] arr1, double[:, :] arr2, int imax, int jmax):

    cdef int i, j
    cdef double norm = 0.0

    for i in range(imax):
        for j in range(jmax):
            norm = norm + fabs( arr1[i, j] - arr2[i, j] )

    return norm

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
cdef inline double norm2(
    double[:, :] arr1, double[:, :] arr2, int imax, int jmax):

    cdef int i, j
    cdef double norm = 0.0

    for i in range(imax):
        for j in range(jmax):
            norm = norm + ( arr1[i, j] - arr2[i, j] ) * ( arr1[i, j] - arr2[i, j] )

    return sqrt(norm)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
cpdef srj(
    np.ndarray[ndim=2, dtype=np.float64_t] phi,
    np.ndarray[ndim=1, dtype=np.float64_t] w,
    np.ndarray[ndim=1, dtype=np.int64_t] q,
    np.ndarray[ndim=2, dtype=np.float64_t] rhs,
    double dx, double dy, int nrows, int ncols, 
    double tol=1e-8, int max_iter=100000):
    r"""Solution using the SRJ algorithm

    Parameters:
    
    phi : numpy.ndarray
        Solution array

    w, q : numpy.ndarray
        SRJ weights and frequency distribution

    rhs : numpy.ndarray
        Poisson source term

    dx, dy : float
        Spatial discretizations along X (cols) & Y (rows)
    
    nrows, ncols : int
        Number of interior nodes or grid points along X/Y

    tol : float
        Convergence tolerance

    max_iter : int
        Maximum number of iterations

    """

    # swap buffer for the SRJ scheme
    cdef np.ndarray[ndim=2, dtype=np.float64_t] phi_new = phi.copy()

    # get the number of cells and index range
    cdef double nnodesi = 1./(nrows*ncols)
    cdef int imax = nrows
    cdef int jmax = ncols

    # get the relaxation schedule
    cdef object srj = SRJScheduler(size=max(nrows, ncols))
    cdef np.ndarray[ndim=1, dtype=np.float64_t] wt = srj.get_relaxation_schedule(w, q)

    # number of 'M' or inner iterations
    cdef int m = wt.size

    # locals
    cdef int i, j, niter_inner, niter_total = 0
    cdef bint break_outer = False
    cdef double error = 1.0
    cdef double omega, omega4

    cdef double dx2i = 1./dx**2
    cdef double dy2i = 1./dy**2
    cdef double denominator = 1.0/(2*dx2i + 2*dy2i)

    while( (error > tol) and (niter_total < max_iter) ):
        # 'M' iterations with prescribed omega
        for niter_inner in range(m):
            niter_total += 1

            omega = wt[ niter_inner ]
            omega4 = 0.25*omega
        
            # update the interior nodes
            for i in range(1, imax-1):
                for j in range(1, jmax-1):
                    phi_new[i, j] = (1.0 - omega)*phi[i, j] + \
                                    ( omega*dx2i*(phi[i-1, j] + phi[i+1, j]) + \
                                      omega*dy2i*(phi[i, j-1] + phi[i, j+1]) - \
                                      omega*rhs[i, j] ) * denominator
                    
            # check for convergence in the 1 Norm and indicate that
            # the inner loop has converged and we want to break
            error = norm1( phi, phi_new, imax, jmax )
            if error < tol:
                break_outer=True
                break

            # swap buffers for next iteration
            phi[:] = phi_new[:]

        # break out of the while loop
        if break_outer: break

    # return the total number of iterations
    return niter_total

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
cpdef gssor(
    np.ndarray[ndim=2, dtype=np.float64_t] phi,
    np.ndarray[ndim=2, dtype=np.float64_t] rhs,
    double dx, double dy, int nrows, int ncols, 
    double tol=1e-12, int max_iter=100000,
    double omega=1.5):
    r"""Solution using the SRJ algorithm

    Parameters:
    
    phi : numpy.ndarray
        Solution array

    rhs : numpy.ndarray
        Poisson source term

    dx, dy : float
        Spatial discretizations along X (cols) & Y (rows)
    
    nrows, ncols : int
        Number of interior nodes or grid points along X/Y

    tol : float
        Convergence tolerance

    max_iter : int
        Maximum number of iterations
    
    omega : double
        SOR relaxation parameter

    """

    # swap buffer for the SRJ scheme
    cdef np.ndarray[ndim=2, dtype=np.float64_t] phi_new = phi.copy()

    # get the number of cells and index range
    cdef double nnodesi = 1./(nrows*ncols)
    cdef int imax = nrows
    cdef int jmax = ncols

    # locals
    cdef int i, j, niter_inner, niter_total = 0
    cdef bint break_outer = False
    cdef double error = 1.0

    cdef double dx2i = 1./dx**2
    cdef double dy2i = 1./dy**2
    cdef double denominator = 1.0/(2*dx2i + 2*dy2i)

    while( (error > tol) and (niter_total < max_iter) ):
        niter_total += 1
        
        # update the interior nodes
        for i in range(1, imax-1):
            for j in range(1, jmax-1):
                phi_new[i, j] = (1.0 - omega)*phi[i, j] + \
                                ( omega*dx2i*(phi_new[i-1, j] + phi[i+1, j]) + \
                                  omega*dy2i*(phi_new[i, j-1] + phi[i, j+1]) - \
                                  omega*rhs[i, j] ) * denominator
                
        # check for convergence and indicate that
        # the inner loop has converged and we want to break
        error = norm1( phi, phi_new, imax, jmax )
        if error < tol:
            break

        # swap buffers for next iteration
        phi[:] = phi_new[:]

    # return the total number of iterations
    return niter_total

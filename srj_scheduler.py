"""Robust Scheduler for the SRJ scheme

This code is adapted from the MATLAB version provided by the authors
in the reference paper "Acceleration of the Jacobi iterative method by
factors exceeding 100 using scheduled relaxation", 2014, JCP, 274, pp
695--708

"""
# NumPy
import numpy

class SRJScheduler(object):
    """Scheduler object"""

    def __init__(self, size=512):
        """Constructor.

        Parameters:
        
        size : int
            Mesh size to be used

        Notes:
        
        The mesh size is used to determine the minimum admissible
        wavenumbers on the grid. 

        """
        # store the mesh size and compute kappa_min
        self.size = size
        self.kmin = kmin = numpy.sin(numpy.pi/2/size)**2

        # use arange to generate the k array initially
        self.k = numpy.arange( kmin, 2.0, kmin )
        
    def set_size(self, size):
        """Set the size and associated kappa_min parameter"""
        self.size = size
        self.kmin = kmin = numpy.sin(numpy.pi/2/size)**2
        self.k = numpy.arange( kmin, 2.0, kmin )

    @classmethod
    def get_relaxation_vectors(self, p=5, size=None):
        """Return the relaxation parameter 'w' and frequency 'q' vectors"""
        if size is None:
            size = self.size
        
        if p == 5:
            if size == 32:
                w = numpy.array( [330.57, 82.172, 13.441, 2.2402, 0.60810] )
                q = numpy.array( [1, 2, 7, 20, 46], int )

            if size == 64:
                w = numpy.array( [1228.8, 220.14, 26.168, 3.1668, 0.63890] )
                q = numpy.array( [1, 3, 10, 38, 106], int )

            if size == 128:
                w = numpy.array( [4522.0, 580.86, 50.729, 4.5018, 0.67161] )
                q = numpy.array( [1, 3, 16, 73, 250], int )

            if size == 256:
                w = numpy.array( [16459, 1513.4, 97.832, 6.4111, 0.70531] )
                q = numpy.array( [1, 4, 26, 142, 605], int )

            # FIXME Verify that this is legal
            if size >= 512:
                w = numpy.array( [59226, 3900.56, 187.53, 9.1194, 0.73905] )
                q = numpy.array( [1, 6, 40, 277, 1500], int )

        elif p == 4:
            if size == 16:
                w = numpy.array( [80.154, 17.217, 2.6201, 0.62230] )
                q = numpy.array( [1, 2, 8, 20], int )
                
            if size == 32:
                w = numpy.array( [289.46, 40.791, 4.0877, 0.66277] )
                q = numpy.array( [1, 3, 14, 46], int )

            if size == 64:
                w = numpy.array( [1029.4, 95.007, 6.3913, 0.70513] )
                q = numpy.array( [1, 5, 26, 114], int )

            if size == 128:
                w = numpy.array( [3596.4, 217.80, 9.9666, 0.74755] )
                q = numpy.array( [1, 7, 50, 285], int )

            if size == 256:
                w = numpy.array( [12329, 492.05, 15.444, 0.78831] )
                q = numpy.array( [1, 9, 86, 664], int )

            if size >= 512:
                w = numpy.array( [41459, 1096.3, 23.730, 0.82597] )
                q = numpy.array( [1, 12, 155, 1650], int )

        elif p == 3:
            if size == 16:
                w = numpy.array( [64.66, 6.215, 0.7042] )
                q = numpy.array( [1, 5, 21], int)

            if size == 32:
                w = numpy.array( [213.8, 11.45, 0.7616] )
                q = numpy.array( [1, 7, 45], int )

            if size == 64:
                w = numpy.array( [684.3, 20.73, 0.8149] )
                q = numpy.array( [1, 11, 106], int )

            if size == 128:
                w = numpy.array( [2114, 36.78, 0.8611] )
                q = numpy.array( [1, 17, 252], int )
                
            if size == 256:
                w = numpy.array( [6319, 63.99, 0.8989] )
                q = numpy.array( [1, 27, 625], int )

            if size == 512:
                w = numpy.array( [18278, 109.2, 0.9282] )
                q = numpy.array( [1, 43, 1571], int )

            if size >= 1024:
                w = numpy.array( [51769.1, 184.31, 0.95025] )
                q = numpy.array( [1, 68, 3955], int )

        else:
            raise ValueError("P level %d not currently supported"%p)

        return w, q
                
    def get_relaxation_schedule(self, w, q, size=None, write2file=False):
        """Return the relaxation schedule for a chosen scheme

        Parameter:
        
        w : Array or list
            Input vector containing the distinct relaxation 
            factors in descending order

        q : Python iterable
            Iteration counts for each relaxation parameter

        size : int
            Optional argument to re-compute the mesh size 
            and associated parameters

        write2file : bool
            Flag to write the relaxation sequence to /tmp/wt.txt

        Returns:

        wt : numpy.ndarray (dim=1)
            Relaxation schedule for the chosen scheme.

        """
        # update mesh size if needed
        if size is not None:
            self.set_size(size)

        # convert the input w and q to numpy arrays
        w = numpy.asarray( w.copy() )
        q = numpy.asarray( q.copy() )

        k = self.k

        # compute the relaxation schedule
        m = numpy.sum(q)
        wt = numpy.zeros(m)
        g = numpy.ones(k.size)

        wt[0] = w[0]
        q[0] = q[0] - 1
        g = g * numpy.abs(1 - k*wt[0])
        
        # begin iterations
        counter = 1; totPerc = 100
        while( sum( q!=0 ) ):
            if ( sum( q==0 ) != 0 ):
                index = numpy.where( q==0 )[0]
                if index == 0:
                    w = w[1:]
                    q = q[1:]
                elif index == q.size-1:
                    w = w[:-1]
                    q = q[:-1]
                else:
                    w[index:-1] = w[index+1:]
                    w = w[:-1]
                    
                    q[index:-1] = q[index+1:]
                    q = q[:-1]
            
            ww = 1./k[ numpy.where( g == numpy.max(g) )[0] ]
            dis = numpy.abs(w - ww)
            index = numpy.where( dis==numpy.min(dis) )[0][0]
            wt[counter] = w[index]
            g = g*numpy.abs( 1 - k*wt[counter] )
            q[index] = q[index] - 1

            # increment counter
            counter += 1
            perc = numpy.sum(q)/wt.size
            
        # write the output to file
        if write2file:
            fname = '/tmp/wt.dat'
            print 'Writing scheduling sequence to %s'%fname
            self.write2file( wt, fname )

        # return the relaxation schedule
        return wt

    def write2file(self, data, filename):
        """Save the data to an ASCII txt file"""
        numpy.savetxt( filename, data )

def test_scheduler(p=3):
    """Simple test for the SRJScheduler 

    We use the input input provided by the authors in the MATLAB
    script for a P=3 scheme on a 16 X 16 grid

    """
    # the srj scheduler object. The data was generated using N=512 for
    # the Matlab script and we follow the same here as well.
    scheduler = SRJScheduler(size=512)
    
    w = numpy.array( [ 64.66, 6.215, 0.7042] )
    q = numpy.array( [1, 5, 21] )

    # get the relaxation schedule generated by this script
    wt = scheduler.get_relaxation_schedule(w, q)

    # compare with sample data
    data = numpy.loadtxt('data/p316.dat')
    
    assert( numpy.alltrue( data == wt ) )

if __name__ == '__main__':
    print 'Testing for P=3, N=16'
    test_scheduler()
    print 'OK'
